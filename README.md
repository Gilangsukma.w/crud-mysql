How to start the server :

1.  Clone this repo into your pc
2.  after you clone the repo, run this on your terminal `cd mysql-crud` then run this `npm install` to install the package on your project
3.  Make branch to prevent conflict when merging the file
4.  Change `MYSQL_USER` and `MYSQL_PASSWORD` value in .env file with your local mysql credential config
5.  You can start the server and do your work, Happy coding :)
