const express = require("express"); // Import express

/* Import controller */
const {
  getAllSuppliers,
  getDetailSupplier,
  createSupplier,
  updateSupplier,
  deleteSupplier,
} = require("../controller/suppliers");

const router = express.Router(); // make express router

/* If user (GET) /, it will go here */
router.get("/", getAllSuppliers);

/* If user (GET) /:id, it will go here */
router.get("/:id", getDetailSupplier);

/* If user (POST) /, it will go here */
router.post("/", createSupplier);

/* If user (PUT) /Supplier/:id, go here */
router.put("/:id", updateSupplier);

/* If user (DELETE) /Supplier/:id, go here */
router.delete("/:id", deleteSupplier);

module.exports = router;
