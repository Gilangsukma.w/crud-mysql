const router = require("express").Router();

const {
  getAllGoods,
  getById,
  addGood,
  updateGood,
  deleteGood,
} = require("../controller/goodsController");

router.get("/", getAllGoods);
router.get("/:id", getById);
router.post("/add", addGood);
router.put("/:id", updateGood);
router.delete("/delete/:id", deleteGood);

module.exports = router;
