const express = require("express"); // Import express

/* Import controller */
const {
  getAllCustomers,
  getDetailCustomer,
  createCustomer,
  updateCustomer,
  deleteCustomer,
} = require("../controller/customers");

const router = express.Router(); // make express router

/* If user (GET) /, it will go here */
router.get("/", getAllCustomers);

/* If user (GET) /:id, it will go here */
router.get("/:id", getDetailCustomer);

// /* If user (POST) /, it will go here */
router.post("/", createCustomer);

// /* If user (PUT) /transaction/:id, go here */
router.put("/:id", updateCustomer);

/* If user (DELETE) /transaction/:id, go here */
router.delete("/:id", deleteCustomer);

module.exports = router;
