const { query } = require("../models");

class Suppliers {
  async getAllSuppliers(req, res) {
    try {
      const data = await query("SELECT id, name FROM suppliers");

      if (!data.length) {
        return res.status(404).json({
          message: "Suppliers not found",
        });
      }

      res.status(200).json({
        data,
      });
    } catch (error) {
      return res.status(500).json({
        message: error.message,
      });
    }
  }

  async getDetailSupplier(req, res) {
    try {
      const data = await query(
        `SELECT id, name FROM suppliers WHERE id=${req.params.id}`
      );

      if (!data.length) {
        return res.status(404).json({
          message: "Supplier not found",
        });
      }

      res.status(200).json({
        data,
      });
    } catch (error) {
      return res.status(500).json({
        message: error.message,
      });
    }
  }

  async createSupplier(req, res) {
    try {
      /* Insert Suppliers */
      const newSupplier = await query(
        `INSERT INTO suppliers (name) VALUES ("${req.body.name}")`
      );

      /* Get the newSupplier data */
      const data = await query(
        `SELECT id, name FROM suppliers WHERE id=${newSupplier.insertId}`
      );

      res.status(201).json({
        data,
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  }

  async updateSupplier(req, res) {
    try {
      /* Find the Supplier */
      const Supplier = await query(
        `SELECT id FROM suppliers WHERE id=${req.params.id}`
      );

      if (!Supplier.length) {
        return res.status(404).json({
          message: "Supplier not found",
        });
      }

      /* Update Suppliers */
      await query(
        `UPDATE suppliers SET name='${req.body.name}' WHERE id=${req.params.id}`
      );

      /* Get the updated Supplier data */
      const data = await query(
        `SELECT id, name FROM suppliers WHERE id=${req.params.id}`
      );

      res.status(201).json({
        data,
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  }

  async deleteSupplier(req, res) {
    try {
      const deletedData = await query(
        `DELETE FROM suppliers WHERE id=${req.params.id}`
      );

      if (deletedData.affectedRows === 0) {
        return res.status(404).json({
          message: "Supplier not found",
        });
      }

      res.status(200).json({
        message: `Supplier with id ${req.params.id} has been deleted`,
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  }
}

module.exports = new Suppliers();
